import os
import pendulum
import geopandas as gpd

import constants as C

class Layer:

    def __init__(self, source:dict) -> None:
        self.name = source["name"] 
        self.filename =  source["filename"] 
        self.date = pendulum.parse(source["date"])
        self.geom_type = None

    def get_layer(self, data_folder:str=C.FILES_FOLDER) -> gpd.GeoDataFrame:
        """Create a GeoDataFrame instance of the given layer. It must be 
        located in the data folder.

        Args:
            data_folder (str): Name of the folder containing the layer file.
        Returns:
            a GeoDataFrame instance of the given file.
        """
        return gpd.read_file(os.path.join(data_folder, self.filename))


class KeyFigures(Layer):

    def __init__(self, source:dict):
        super().__init__(source)
        self.category_column = source["columns"].get("category", None)
        self.measure_column = source["columns"].get("measure", None)
        self.date_column = source["columns"].get("date", None)
        self.output_filename = source["output"].get("filename", None)
        self.output_folder = (
            source["output"].get("destination_folder", None)
        )


class Communes(Layer):

    def __init__(self, source: dict) -> None:
        super().__init__(source)
        self.commune_code_column = source["columns"].get("territory_code", None)