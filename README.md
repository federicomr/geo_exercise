# **Exercise: calculating key figures**

## **Requirements**
- python 3.8+.
- A communes shape file (you can get it from [HERE](https://data.geopf.fr/telechargement/download/ADMIN-EXPRESS/ADMIN-EXPRESS_3-2__SHP_LAMB93_FXX_2024-04-23/ADMIN-EXPRESS_3-2__SHP_LAMB93_FXX_2024-04-23.7z))
- A shapefile of the key figures to work with. 
- For this exercise we used the following:
  - As polygons layer we used this [natural preservation areas](https://inpn.mnhn.fr/docs/Shape/znieff1.zip)
  - As lines layer we used the [National Railways Network of France](https://www.data.gouv.fr/en/datasets/fichier-de-formes-des-voies-du-reseau-ferre-national/)
  - And as points layer this [educational institutions shape](https://www.data.gouv.fr/fr/datasets/adresse-et-geolocalisation-des-etablissements-denseignement-du-premier-et-second-degres-1/)

## **Installation**
1. Create a python virtual environment to run th proccess.
```shell
python -m venv env
```

2. Activate the virtual environment.
```shell
source env/bin/activate
```

3. Install python dependencies
```shell
pip install -r requirements.txt
```

4. Make sure to put the shapefiles in the files folder inside the project folder. 

5. Fill in the constants variables in `constants.py`.
6. Create a `config.json` in the main project folder and fill it with the source layer info.
```shell
cp config-template.txt config.json
```
As an example, we left the three config files for the three used shapes in the `config_examples` folder.

## **Execution**
To execute the process you should run the run.py in the command line. It accepts two optional parameters: config-file and category.
To run the process with the default configuration
```shell
python main.py
```

To run the process specifying the config file you should use the `config-file` option
```shell
python main.py -f <custom-config-file.json>
```

To run the process and calculate the metrics by category use the `category` option. By default is `False`.
```shell
python main.py -c True
```

As result, it will export a new shapefile in the files folder with the name specified in the config json file.


