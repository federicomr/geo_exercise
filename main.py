import os
import pandas as pd
import geopandas as gpd
import json
import click

from classes import Communes, KeyFigures
import constants as C


def preprocess_geometries(key_figures:gpd.GeoDataFrame) -> gpd.GeoDataFrame:
    """Clean and prepair the key figures geometries before running the process.

    Args:
        key_figures (gpd.GeoDataFrame): The key figures data and geometries

    Returns:
        gpd.GeoDataFrame: The key figures layer ready to run the process.
    """
    # Remove objects without geometry if there are any
    key_figures_result = key_figures.dropna(subset=["geometry"]).copy()

    # Reproject the layer to the defined crs
    key_figures_result = key_figures_result.to_crs(C.PROJECT_CRS)

    # Explode the geometries to avoid multi geom type
    key_figures_result = key_figures_result.explode(ignore_index=True)

    # Get geom type and save it as KeyFigures attribute
    geometry_types = key_figures_result.geom_type.unique()
    if geometry_types.size > 1:
        raise IndexError(f"The layer contains more than one geom type {geometry_types}")
    else:
        KEY_FIGURES.geom_type = geometry_types[0]

    return key_figures_result


def get_measure_column(key_figures_commune:gpd.GeoDataFrame, 
                       geom_type:str) -> pd.Series:
    """calculates the measurement corresponding to the layer of key figures
    by commune considering the type of geometry 

    Args:
        key_figures_commune (gpd.GeoDataFrame): The key figures by commune.
        geom_type (str): The type of geometries from the key figures data.

    Returns:
        pd.Series: A column with the calculated measure
    """
    if geom_type == "Polygon":
        # If it's Polygon, calculate the surface in HA
        measure_column = key_figures_commune["geometry"].area / 10000  

    elif geom_type == "LineString":
        # If it's LineString calculate the length in meters
        measure_column = key_figures_commune["geometry"].length
        

    elif geom_type == "Point":    
        measure_column = (
            key_figures_commune.groupby("INSEE_COM")
            .agg({"geometry":"count"})
            .squeeze()
        )
    
    else:
        raise ValueError(f"Invalid geom_type {geom_type}.")
    
    return measure_column


def get_ha_by_commune(communes_layer:Communes, 
                      key_figures_layer:KeyFigures, 
                      category_value:str=None) -> pd.DataFrame:
    """Split and dissolve the entity layer polygons to get the total entity 
    area for each commune.

    Args:
        communes (Communes): An object with the data of the communes layer.
        entity_layer (KeyFigures): An object with the data to work with a key
        figures layer. 
        category_value(str): Value of the category column from the key figures
        if they have.

    Returns:
        gpd.GeoDataFrame: A GeoDataFrame layer of the entity polygons group by
        commune. It includes a column with the total surface in ha of each 
        group.
    """

    # First split each natural area polygon based on communes polygons.  
    entity_splitted = key_figures_layer.overlay(
        communes_layer, 
        how="intersection", 
        keep_geom_type=True
        )
    
    # Define column name for measurement values
    if category_value is not None:
        measure_column = KEY_FIGURES.measure_column + "_" + category_value
    else:
        measure_column = KEY_FIGURES.measure_column
    
    # Second, agreggate all natural area polygon by commune
    # Avoid aggregation if the geom type is point because is useless to
    # count the points by commune.
    # NOTE: This if/elif could be remove when the method 'count_geometries'
    # of 1.0 version of GeoPandas is implemented. 
    if KEY_FIGURES.geom_type != "Point":

        entity_commune = (
            entity_splitted.dissolve(by=COMMUNES.commune_code_column)
            .reset_index()
        )

        # Third, calculate the area in ha of the total entity polygons in 
        # each commune
        entity_commune[measure_column] = (
            get_measure_column(entity_commune, KEY_FIGURES.geom_type)
        )
    
    elif KEY_FIGURES.geom_type == "Point":
        entity_commune = pd.DataFrame(
            get_measure_column(entity_splitted, KEY_FIGURES.geom_type)
            .rename(measure_column)
            ).reset_index()

    # Join the entity polygons by commune layer with the complete 
    # communes layer
    entity_commune_complete = pd.merge(
        communes_layer[[COMMUNES.commune_code_column]],
        entity_commune[[COMMUNES.commune_code_column, measure_column]],
        on=COMMUNES.commune_code_column,
        how="left"
    ).set_index(COMMUNES.commune_code_column)

    # Fill the values of communes without entity polygons
    entity_commune_complete[measure_column] = (
        entity_commune_complete[measure_column].fillna(0).astype(int)
    )

    # Finally, return a DataFrame with the communes ID and the area in ha
    return entity_commune_complete



def run_processs(category:bool=False):
    """Executes the processs of getting the key_figures area by communes.

    Args:
        category (bool): If is True the processs will calculate the total 
        surface of key figures by category.

    Returns:
        pd.DataFrame: A table of Communes with the total surface of key 
        figures.
    """

    # Get layers
    communes_layer = COMMUNES.get_layer()
    key_figures_layer = KEY_FIGURES.get_layer()

    # Preprocess the key figures layer
    key_figures_layer = preprocess_geometries(key_figures_layer)

    if category is True:
        # If the key figures file includes a category column calculate the 
        # surface by each category value.
        partial_dfs = []

        for category_value in key_figures_layer[KEY_FIGURES.category_column].unique():
            # Get a DataFrame of each category value
            key_figures_category = (
                key_figures_layer.groupby(KEY_FIGURES.category_column)
                .get_group(category_value)
            )

            communes_area_category = (
                get_ha_by_commune(communes_layer, 
                                  key_figures_category, 
                                  category_value)
            )

            partial_dfs.append(communes_area_category)
            communes_figures_area = pd.concat(partial_dfs, 
                                              axis=1).reset_index()
    else:
        communes_figures_area = (
            get_ha_by_commune(communes_layer,key_figures_layer)
            .reset_index()
        )

    # Add date column
    communes_figures_area[KEY_FIGURES.date_column] = (
        KEY_FIGURES.date.to_date_string()
    )

    # Rename the territory code column
    communes_figures_area = (
        communes_figures_area.rename(
            columns={COMMUNES.commune_code_column:"codgeo"}
            )
    )

    return communes_figures_area


def persist_data(dataset:pd.DataFrame, with_category:bool=False) -> None:
    """Persist the result in the destination file. It must be in the files 
    folder.

    Args:
        source_file (gpd.GeoDataFrame): The result data to persist.
        destination_file (str, optional): Name and extension of the 
        destination file. Defaults to C.RESULT_DESTINATION_FILE.
    """

    def get_output_filename(with_category:bool=False) -> str:
        """Gets the output filename path defined in the config file.

        Args:
            with_category (bool, optional): Set it to True if the output file 
            includes the category columns. Defaults to False.

        Returns:
            str: the output filename path.
        """
        # Define the destination file name
        output_filename = KEY_FIGURES.output_filename.format(
            KEY_FIGURES.date.year,
            COMMUNES.date.year
        )

        # Define the destination folder
        if with_category is True:
            output_folder = KEY_FIGURES.output_folder + "_category"
        else:
            output_folder = KEY_FIGURES.output_folder

        # Generate file path
        output_file_path = os.path.join(
            C.FILES_FOLDER,
            output_folder,
            output_filename
        )

        # Create folders if not exists
        os.makedirs(os.path.dirname(output_file_path), exist_ok=True)

        return output_file_path

    output_file_path = get_output_filename(with_category)
    dataset.to_csv(output_file_path, index=False)


@click.command()
@click.option("-f", "--config-file", 
              default=C.CONFIG_FILE, 
              help="""Name of the json config file. By default is the file 
              defined in the CONFIG_FILE constant.""")
@click.option("-c", "--category", 
              default=False, 
              type=bool, 
              help="""True if you want to get the output splitted 
              by category.""")
def run(config_file, category):

    # Read the configuration file with the layers data
    with open(config_file, "r") as json_file:
        layer_data = json.load(json_file)

    # Instance an object for each layer the key figures and the communes
    # of France.
    global KEY_FIGURES, COMMUNES
    KEY_FIGURES = KeyFigures(layer_data["key_figures"])
    COMMUNES = Communes(layer_data["communes"])

    # Run processs 
    communes_figures_area = run_processs(category)

    # Persist data in local file
    persist_data(communes_figures_area, category)

if __name__ == "__main__":
    run()
    