{
    "key_figures": {
        "name": "",
        "filename": "",
        "date": "",
        "columns": {
            "category": "",
            "measure": "",
            "date": ""
        },
        "output": {
            "destination_folder": "",
            "filename": ""
        } 
    },
    "communes": {
        "name": "communes",
        "filename": "COMMUNE.shp",
        "date": "2024-04-01",
        "columns": {
            "territory_code": "INSEE_COM"
        }
    }
}